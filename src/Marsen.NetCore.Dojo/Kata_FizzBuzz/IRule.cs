﻿namespace Marsen.NetCore.Dojo.Kata_FizzBuzz
{
    public interface IRule
    {
        string Apply(int input, string result);
    }
}