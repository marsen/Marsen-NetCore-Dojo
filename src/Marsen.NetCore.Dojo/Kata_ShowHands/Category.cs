﻿namespace Marsen.NetCore.Dojo.Kata_ShowHands
{
    public enum Category
    {
        HighCard,
        OnePair,
        TwoPair,
        ThreeOfAKind,
        Straight,
        Flush,
        FullHouse,
        FourOfAKind,
        StraightFlush,
    }
}