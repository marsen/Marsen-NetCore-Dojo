﻿using System;

namespace Marsen.NetCore.Dojo.Kata_JsonParser
{
    public class PersonaOriginEntity
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime BirthDate { get; set; }
    }
}