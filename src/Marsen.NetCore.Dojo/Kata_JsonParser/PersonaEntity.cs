﻿namespace Marsen.NetCore.Dojo.Kata_JsonParser
{
    public class PersonaEntity
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}