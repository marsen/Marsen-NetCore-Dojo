﻿namespace Marsen.NetCore.Dojo.Kata_Api_Pay.Interface
{
    public interface IConfigure
    {
        string Setting(string key);
    }
}