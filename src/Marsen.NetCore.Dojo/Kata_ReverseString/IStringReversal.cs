﻿namespace Marsen.NetCore.Dojo.Tests.Kata_ReverseString
{
    public interface IStringReversal
    {
        string Do(string input);
    }
}