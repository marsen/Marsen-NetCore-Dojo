﻿

## Test Case

| Joey     | Tom | Expceted |
| -------- | -------- | -------- |
| 0     | 0     | Love All     |
| 1     | 0     | Fifteen Love     |
| 2     | 0     | Thirty Love     |
| 3     | 0     | Forty Love     |
| 0     | 1     | Love Fifteen    |
| 0     | 2     | Love Thirty    |
| 0     | 3     | Love Forty    |
| 1     | 1     | Fifteen All     |
| 2     | 2     | Thirty All     |
| 3     | 3     | Duece        |
| 4     | 4     | Duece        |
| 4     | 3     | Joey Adv     |
| 5     | 3     | Joey Win     |
| 3     | 4     | Tom Adv     |
| 3     | 5     | Tom Win     |