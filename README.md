# Marsen.NetCore.Dojo

[![Build Status](https://travis-ci.com/marsen/Marsen.NetCore.Dojo.svg?branch=master)](https://travis-ci.com/marsen/Marsen.NetCore.Dojo)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=marsen_Marsen.NetCore.Dojo&metric=alert_status)](https://sonarcloud.io/dashboard?id=marsen_Marsen.NetCore.Dojo)

For Kata

## Goal

- Refator
  - bad smell
  - design pattern
  - issue analysis
  - workingProcess
- Vim
  - Resharp
  - typing
- Pair
  - Learn from others  
- CI/CD Tools
  - Github Action
  - Travis CI
  - SounarCloud
